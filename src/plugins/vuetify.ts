import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#ff9907',
    secondary: '#cddc39',
    accent: '#673ab7',
    error: '#ff5722',
    warning: '#795548',
    info: '#009688',
    success: '#4caf50',
  },
});
