import Vue from 'vue';
import Router from 'vue-router';
import { Auth } from 'aws-amplify';
import { CognitoUser } from '@aws-amplify/auth';
const Login = () => import('@/views/Login.vue');
const MainFrame = () => import('@/views/MainFrame.vue');
const History = () => import('@/views/History.vue');
const NewEmail = () => import('@/views/NewEmail.vue');

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { requiresNotAuth: true },
    },
    {
      path: '/',
      name: 'MainFrame',
      component: MainFrame,
      redirect: { name: 'History' },
      meta: { requiresAuth: true },
      children: [
        {
          path: 'history',
          name: 'History',
          component: History,
        },
        {
          path: 'new_email',
          name: 'NewEmail',
          component: NewEmail,
        },
      ],
    },
  ],
});

router.beforeEach((to, from, next) => {
  Auth.currentAuthenticatedUser()
    .then((user) => {
      if (to.matched.some((m) => m.meta.requiresNotAuth)) {
        if (user instanceof CognitoUser) {
          console.log(`User already signed in: ${user.getUsername()}`);
        }
        next({ name: 'MainFrame' });
      }
      next();
    })
    .catch((err) => {
      if (to.matched.some((m) => m.meta.requiresAuth)) {
        console.log('User not signed in');
        next({ name: 'Login' });
      }
      next();
    });
});

export default router;
