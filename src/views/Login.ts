import { Component, Vue } from 'vue-property-decorator';
import { Auth } from 'aws-amplify';
import { CognitoUser } from '@aws-amplify/auth';
import { AmplifyEventBus } from 'aws-amplify-vue';
import { AuthStateType } from '@/interfaces/AuthStateType';

@Component
export default class Login extends Vue {
  protected authConfig = {
    signUpConfig: {
      hiddenDefaults: ['phone_number'],
      signUpFields: [
        {
          label: 'Name',
          key: 'name',
          required: true,
          displayOrder: '0',
          type: 'string',
        },
      ],
    },
  };

  protected mounted() {
    Auth.currentAuthenticatedUser().then((user) => {
      if (user instanceof CognitoUser) {
        console.log(`User already signed in: ${user.getUsername()}`);
      }
      this.$router.push({name: 'MainFrame'});
    });
    AmplifyEventBus.$on('authState', (info: AuthStateType) => {
      if (info === 'signedIn') {
        console.log('User Signed In');
        this.$router.push({name: 'MainFrame'});
      }
    });
  }
}
