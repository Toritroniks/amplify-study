import { Component, Vue } from 'vue-property-decorator';
import { Auth, API } from 'aws-amplify';
import { CognitoUser } from '@aws-amplify/auth';
import VueMarkdown from 'vue-markdown';

@Component({
  components: {
    VueMarkdown,
  },
})
export default class NewEmail extends Vue {
  protected to: string = '';
  protected subject: string = '';
  protected rawBody: string = '';
  private htmlBody: string = '';
  private userInfo: any | null = null;

  protected async mounted() {
    this.userInfo = await Auth.currentUserInfo();
  }

  protected updateHtmlBody(outHtml: string) {
    this.htmlBody = outHtml;
  }

  protected sendMarkdownEmail() {
    if (this.userInfo) {
      API.get('SendMarkdownEmailApi', '/send', {
        queryStringParameters: {
          username: this.userInfo.username,
          from: this.userInfo.attributes.email,
          to: this.to,
          subject: this.subject,
          html: this.htmlBody,
        },
      })
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });

    }
  }
}
