import { Component, Vue } from 'vue-property-decorator';
import { Auth } from 'aws-amplify';

@Component
export default class MainFrame extends Vue {
  protected signOut() {
    Auth.signOut()
      .then(() => {
        console.log('User Signed Out');
        this.$router.push({ name: 'Login' });
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
