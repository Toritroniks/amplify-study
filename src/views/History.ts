import { Component, Vue } from 'vue-property-decorator';
import { Auth, graphqlOperation } from 'aws-amplify';
import { listEmails } from '@/graphql/queries';
import { onCreateEmail } from '@/graphql/subscriptions';

@Component
export default class History extends Vue {
  private userInfo: any | null = null;

  protected async mounted() {
    this.userInfo = await Auth.currentUserInfo();
  }

  get emailsQuery() {
    return graphqlOperation(listEmails, { filter: { username: { eq: this.userInfo.username } } });
  }

  get emailsSubscription() {
    return graphqlOperation(onCreateEmail);
  }

  protected onSubscriptionMsg(prevData: any, newData: any) {
    prevData.data.listEmails.items.push(newData.onCreateEmail);
    return prevData.data;
  }

  protected getDisplayDate(unixTime: number) {
    const date = new Date(unixTime * 1000);
    const dateStr = `${date.getFullYear()}/${date.getMonth()}/${date.getDate()}`;
    const timeStr = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    return `${dateStr} ${timeStr}`;
  }
}
