// tslint:disable
// this is an auto generated file. This will be overwritten

export const getEmail = `query GetEmail($id: ID!) {
  getEmail(id: $id) {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
export const listEmails = `query ListEmails(
  $filter: ModelEmailFilterInput
  $limit: Int
  $nextToken: String
) {
  listEmails(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      username
      subject
      toList
      sendDate
      body
    }
    nextToken
  }
}
`;
