// tslint:disable
// this is an auto generated file. This will be overwritten

export const createEmail = `mutation CreateEmail($input: CreateEmailInput!) {
  createEmail(input: $input) {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
export const updateEmail = `mutation UpdateEmail($input: UpdateEmailInput!) {
  updateEmail(input: $input) {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
export const deleteEmail = `mutation DeleteEmail($input: DeleteEmailInput!) {
  deleteEmail(input: $input) {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
