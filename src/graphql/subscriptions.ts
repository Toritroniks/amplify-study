// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateEmail = `subscription OnCreateEmail {
  onCreateEmail {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
export const onUpdateEmail = `subscription OnUpdateEmail {
  onUpdateEmail {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
export const onDeleteEmail = `subscription OnDeleteEmail {
  onDeleteEmail {
    id
    username
    subject
    toList
    sendDate
    body
  }
}
`;
