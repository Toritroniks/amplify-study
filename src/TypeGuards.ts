import { ListEmailsQuery } from '@/EmailGQL';

export default class TypeGuards {
  public static isListEmailsQuery(data: object | undefined): data is ListEmailsQuery {
    return (data as ListEmailsQuery).listEmails !== undefined;
  }
}
