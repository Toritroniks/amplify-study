/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateEmailInput = {
  id?: string | null,
  username: string,
  subject: string,
  toList: Array< string >,
  sendDate: string,
  body?: string | null,
};

export type UpdateEmailInput = {
  id: string,
  username?: string | null,
  subject?: string | null,
  toList?: Array< string > | null,
  sendDate?: string | null,
  body?: string | null,
};

export type DeleteEmailInput = {
  id?: string | null,
};

export type ModelEmailFilterInput = {
  id?: ModelIDFilterInput | null,
  username?: ModelStringFilterInput | null,
  subject?: ModelStringFilterInput | null,
  toList?: ModelStringFilterInput | null,
  sendDate?: ModelStringFilterInput | null,
  body?: ModelStringFilterInput | null,
  and?: Array< ModelEmailFilterInput | null > | null,
  or?: Array< ModelEmailFilterInput | null > | null,
  not?: ModelEmailFilterInput | null,
};

export type ModelIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type CreateEmailMutationVariables = {
  input: CreateEmailInput,
};

export type CreateEmailMutation = {
  createEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type UpdateEmailMutationVariables = {
  input: UpdateEmailInput,
};

export type UpdateEmailMutation = {
  updateEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type DeleteEmailMutationVariables = {
  input: DeleteEmailInput,
};

export type DeleteEmailMutation = {
  deleteEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type GetEmailQueryVariables = {
  id: string,
};

export type GetEmailQuery = {
  getEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type ListEmailsQueryVariables = {
  filter?: ModelEmailFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListEmailsQuery = {
  listEmails:  {
    __typename: "ModelEmailConnection",
    items:  Array< {
      __typename: "Email",
      id: string,
      username: string,
      subject: string,
      toList: Array< string >,
      sendDate: string,
      body: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateEmailSubscription = {
  onCreateEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type OnUpdateEmailSubscription = {
  onUpdateEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};

export type OnDeleteEmailSubscription = {
  onDeleteEmail:  {
    __typename: "Email",
    id: string,
    username: string,
    subject: string,
    toList: Array< string >,
    sendDate: string,
    body: string | null,
  } | null,
};
