export type AuthStateType = 'signedIn' | 'confirmSignIn' | 'forgotPassword' | 'signUp';
